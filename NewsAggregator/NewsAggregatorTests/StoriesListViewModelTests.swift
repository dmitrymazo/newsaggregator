//
//  StoriesListViewModelTests.swift
//  NewsAggregatorTests
//
//  Created by Dmitry on 24.03.2022.
//

import XCTest
@testable
import NewsAggregator

final class StoriesListViewModelTests: XCTestCase {
    
    private var stories: [Story]!
    private var storyNetworkService: MockStoryNetworkService!
    private var imageNetworkService: MockImageNetworkService!
    private var viewModel: StoriesListViewModel!
    
    override func setUp() {
        super.setUp()
        stories = [
            Story(title: "1", subtitle: "11", imageUrl: "111"),
            Story(title: "2", subtitle: "22", imageUrl: "222")
        ]
        
        storyNetworkService = MockStoryNetworkService()
        imageNetworkService = MockImageNetworkService()
        let dispatcher = MockDispatcher()
        viewModel = StoriesListViewModel(storyNetworkService: storyNetworkService,
                                         imageNetworkService: imageNetworkService,
                                         dispatcher: dispatcher)
    }
    
    override func tearDown() {
        stories = []
        storyNetworkService = nil
        imageNetworkService = nil
        viewModel = nil
        super.tearDown()
    }
    
    func testStoriesAreLoaded()  {
        XCTAssertEqual(viewModel.stories.count, 0)
        viewModel.load()
        storyNetworkService.invoke(stories: stories)
        XCTAssertEqual(viewModel.stories.count, 2)
    }
    
    func testImagesAreLoaded()  {
        XCTAssertEqual(viewModel.storyImages.count, 0)
        viewModel.load()
        storyNetworkService.invoke(stories: stories)
        imageNetworkService.invoke(image: UIImage())
        XCTAssertEqual(viewModel.storyImages.count, 2)
    }
    
}
