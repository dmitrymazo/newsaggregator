//
//  MockDispatcher.swift
//  NewsAggregatorTests
//
//  Created by Dmitry on 25.03.2022.
//

@testable
import NewsAggregator
import Foundation

final class MockDispatcher: Dispatcher {    
    func async(execute: @escaping () -> Void) {
        execute()
    }
    
    func async(after deadline: DispatchTime, execute: @escaping () -> Void) {
        execute()
    }
}
