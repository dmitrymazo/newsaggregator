//
//  MockCachingService.swift
//  NewsAggregatorTests
//
//  Created by Dmitry on 25.03.2022.
//

@testable
import NewsAggregator
import Foundation

final class MockCachingService: StoryCacher {
    private var savedStories = [Story]()
    
    func stories() -> [Story] {
        return savedStories
    }
    
    func save(stories: [Story]) {
        savedStories = stories
    }
}
