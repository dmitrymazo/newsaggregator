//
//  MockStoryNetworkService.swift
//  NewsAggregatorTests
//
//  Created by Dmitry on 25.03.2022.
//

import Combine
import Foundation
@testable
import NewsAggregator
import UIKit

final class MockStoryNetworkService: StoriesListNetworkService {
    private let subject = PassthroughSubject<[Story], Error>()
    
    func stories() -> AnyPublisher<[Story], Error> {
        subject.eraseToAnyPublisher()
    }
    
    func invoke(stories: [Story]) {
        subject.send(stories)
    }
}

final class MockImageNetworkService: ImageNetworkService {
    private let subject = PassthroughSubject<UIImage, Error>()
    
    func loadImage(fromUrl url: String) -> AnyPublisher<UIImage, Error> {
        subject.eraseToAnyPublisher()
    }
    
    func invoke(image: UIImage) {
        subject.send(image)
    }
}
