//
//  CacheOrNetworkStoryLoaderTests.swift
//  NewsAggregatorTests
//
//  Created by Dmitry on 25.03.2022.
//

@testable
import NewsAggregator
import XCTest
import Combine

final class CacheOrNetworkStoryLoaderTests: XCTestCase {
    
    private var networkService: MockStoryNetworkService!
    private var cachingService: MockCachingService!
    private var loader: CacheOrNetworkStoryLoader!
    private var cancellables = Set<AnyCancellable>()
    
    override func setUp() {
        super.setUp()
        networkService = MockStoryNetworkService()
        cachingService = MockCachingService()
        
        loader = CacheOrNetworkStoryLoader(networkService: networkService,
                                           cachingService: cachingService)
    }
    
    override func tearDown() {
        networkService = nil
        cachingService = nil
        loader = nil
        cancellables = []
        super.tearDown()
    }
    
    func testHasCachedItems_LoadFromCache() {
        let cachedItems = [
            Story(title: "1", subtitle: "11", imageUrl: "111"),
            Story(title: "2", subtitle: "22", imageUrl: "222")
        ]
        let networkItems = [
            Story(title: "1", subtitle: "11", imageUrl: "111"),
            Story(title: "2", subtitle: "22", imageUrl: "222")
        ]
        
        cachingService.save(stories: cachedItems)
        networkService.invoke(stories: networkItems)
        
        var result = [Story]()
        loader.stories()
            .sink { _ in } receiveValue: { stories in
                result = stories
            }.store(in: &cancellables)
        
        XCTAssertEqual(result, cachedItems)
    }
    
    func testHasNoCachedItems_LoadFromNetwork() {
        let networkItems = [
            Story(title: "1", subtitle: "11", imageUrl: "111"),
            Story(title: "2", subtitle: "22", imageUrl: "222")
        ]
        
        var result = [Story]()
        loader.stories()
            .sink { _ in } receiveValue: { stories in
                result = stories
            }.store(in: &cancellables)
        
        networkService.invoke(stories: networkItems)
        
        XCTAssertEqual(result, networkItems)
    }
    
}
