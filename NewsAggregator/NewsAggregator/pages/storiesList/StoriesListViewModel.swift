//
//  StoriesListViewModel.swift
//  NewsAggregator
//
//  Created by Dmitry on 24.03.2022.
//

import Combine
import Foundation
import UIKit

final class StoriesListViewModel: ObservableObject {
    
    private let storyNetworkService: StoriesListNetworkService
    private let imageNetworkService: ImageNetworkService
    
    @Published
    private(set) var stories = [Story]()
    
    @Published
    private(set) var storyImages = [String: UIImage]()
    
    private let dispatcher: Dispatcher
    
    private var cancellables = Set<AnyCancellable>()
    
    private func loadImage(fromUrl url: String, title: String) {
        imageNetworkService.loadImage(fromUrl: url)
            .sink { result in
                switch result {
                case .failure(let error):
                    print(error)
                default:
                    break
                }
            } receiveValue: { [weak self] image in
                self?.dispatcher.async {
                    guard self?.storyImages[url] == nil else { return }
                    self?.storyImages[url] = image
                }
            }.store(in: &cancellables)
    }
    
    private func loadImages(forStories stories: [Story]) {
        stories.forEach { self.loadImage(fromUrl: $0.imageUrl, title: $0.title) }
    }
    
    func load() {
        storyNetworkService.stories()
            .sink { result in
                switch result {
                case .failure(let error):
                    print(error)
                default:
                    break
                }
            } receiveValue: { [weak self] stories in
                self?.loadImages(forStories: stories)
                self?.dispatcher.async {
                    self?.stories = stories
                }
            }.store(in: &cancellables)
    }
    
    init(storyNetworkService: StoriesListNetworkService,
         imageNetworkService: ImageNetworkService,
         dispatcher: Dispatcher) {
        self.storyNetworkService = storyNetworkService
        self.imageNetworkService = imageNetworkService
        self.dispatcher = dispatcher
    }
    
}
