//
//  StoriesListView.swift
//  NewsAggregator
//
//  Created by Dmitry on 24.03.2022.
//

import SwiftUI

struct StoriesListView: View {
    
    @ObservedObject
    private var viewModel: StoriesListViewModel
    
    private var storyCells: some View {
        VStack {
            Text("count: \(viewModel.stories.count)")
            List(viewModel.stories, id: \.self) { story in
                storyCell(story: story)
            }
        }
        .refreshable {
            viewModel.load()
        }
    }
    
    @ViewBuilder
    private func storyCell(story: Story) -> some View {
        if let image = viewModel.storyImages[story.imageUrl] {
            StoryCellView(story: story, image: image)
        } else {
            StoryCellView(story: story)
        }
    }
    
    var body: some View {
        storyCells
            .onAppear {
                viewModel.load()
            }
    }
    
    init() {
        let storyNetworkService = AggregatedStoriesListNetworkService()
        let imageNetworkService = DefaultImageNetworkService()
        let dispatcher = DefaultDispatchMain()
        viewModel = StoriesListViewModel(storyNetworkService: storyNetworkService,
                                         imageNetworkService: imageNetworkService,
                                         dispatcher: dispatcher)
    }
    
}
