//
//  StoryCellView.swift
//  NewsAggregator
//
//  Created by Dmitry on 24.03.2022.
//

import SwiftUI

struct StoryCellView: View {
    
    let story: Story
    let image: UIImage?
    
    @ViewBuilder
    private var imageView: some View {
        if let image = image {
            Image(uiImage: image)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 70, height: 70, alignment: .center)
                .clipped()
                .cornerRadius(20)
        } else {
            Image(systemName: "info.circle")
        }
    }
    
    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 10) {
                Text(story.title)
                Text(story.subtitle)
                    .multilineTextAlignment(.leading)
            }
            Spacer()
            imageView
        }
        .frame(width: 300, height: 150)
        .padding(10)
        .border(Color.black, width: 2)
    }
    
    init(story: Story) {
        self.story = story
        self.image = nil
    }
    
    init(story: Story, image: UIImage) {
        self.story = story
        self.image = image
    }
    
}
