//
//  Dispatcher.swift
//  NewsAggregator
//
//  Created by Dmitry on 25.03.2022.
//

import Foundation

/// A wrapper around DispatchQueue for the purpose of making async requests synchronous in tests
public protocol Dispatcher {
    func async(execute: @escaping () -> Void)
    func async(after deadline: DispatchTime, execute: @escaping () -> Void)
}

public class DefaultDispatcher: Dispatcher {
    
    private let queue: DispatchQueue
    
    public init(queue: DispatchQueue) {
        self.queue = queue
    }
    
    public func async(execute: @escaping () -> Void) {
        self.queue.async(execute: execute)
    }
    
    public func async(after deadline: DispatchTime, execute: @escaping () -> Void) {
        self.queue.asyncAfter(deadline: deadline, execute: execute)
    }
}

public final class DefaultDispatchMain: DefaultDispatcher {
    public init() {
        super.init(queue: DispatchQueue.main)
    }
}
