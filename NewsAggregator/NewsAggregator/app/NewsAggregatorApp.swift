//
//  NewsAggregatorApp.swift
//  NewsAggregator
//
//  Created by Dmitry on 24.03.2022.
//

import SwiftUI

struct NewsAggregatorApp: App {
    var body: some Scene {
        WindowGroup {
            StoriesListView()
        }
    }
}
