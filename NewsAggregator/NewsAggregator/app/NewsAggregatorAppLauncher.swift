//
//  NewsAggregatorAppLauncher.swift
//  NewsAggregator
//
//  Created by Dmitry on 25.03.2022.
//

import SwiftUI

@main
struct NewsAggregatorAppLauncher {
    
    static func main() throws {
        if NSClassFromString("XCTestCase") == nil {
            NewsAggregatorApp.main()
        } else {
            NewsAggregatorTestApp.main()
        }
    }
    
}
