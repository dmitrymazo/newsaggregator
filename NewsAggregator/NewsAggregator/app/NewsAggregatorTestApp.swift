//
//  NewsAggregatorTestApp.swift
//  NewsAggregator
//
//  Created by Dmitry on 25.03.2022.
//

import SwiftUI

struct NewsAggregatorTestApp: App {
    var body: some Scene {
        WindowGroup { Text("Running Unit Tests") }
    }
}
