//
//  Cacher.swift
//  NewsAggregator
//
//  Created by Dmitry on 25.03.2022.
//

import Combine
import CoreFoundation
import Foundation
import KituraCache

protocol StoryCacher {
    func stories() -> [Story]
    func save(stories: [Story])
}

final class DefaultStoryCacher: StoryCacher {
    
    private let serviceName: String
    private let staleTimeout: UInt
    private let cache = KituraCache()
    
    func stories() -> [Story] {
        return (cache.object(forKey: serviceName) as? [Story]) ?? []
    }
    
    func save(stories: [Story]) {
        self.cache.setObject(stories, forKey: serviceName, withTTL: staleTimeout)
    }
    
    init(serviceName: String, staleTimeout: UInt) {
        self.serviceName = serviceName
        self.staleTimeout = staleTimeout
    }
}
