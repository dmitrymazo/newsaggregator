//
//  CacheOrNetworkStoryLoader.swift
//  NewsAggregator
//
//  Created by Dmitry on 25.03.2022.
//

import Combine

final class CacheOrNetworkStoryLoader: StoriesListNetworkService {
    
    private let networkService: StoriesListNetworkService
    private let cachingService: StoryCacher
    private var networkSubject = PassthroughSubject<[Story], Error>()
    private var cancellables = Set<AnyCancellable>()
    
    private func setupPublisher() {
        networkService.stories()
            .sink { [weak self] result in
                switch result {
                case .failure(_):
                    self?.networkSubject.send(completion: result)
                default:
                    break
                }
            } receiveValue: { [weak self] stories in
                self?.networkSubject.send(stories)
                self?.cachingService.save(stories: stories)
            }.store(in: &cancellables)
    }
    
    func stories() -> AnyPublisher<[Story], Error> {
        let cachedStories = cachingService.stories()
        guard !cachedStories.isEmpty else {
            self.setupPublisher()
            return networkSubject.eraseToAnyPublisher()
        }
        return Just(cachedStories).mapError { _ in
            CustomError.defaultError
        }.eraseToAnyPublisher()
    }
    
    func save(stories: [Story]) {
        cachingService.save(stories: stories)
    }
    
    init(networkService: StoriesListNetworkService,
         cachingService: StoryCacher) {
        self.networkService = networkService
        self.cachingService = cachingService
    }
    
}
