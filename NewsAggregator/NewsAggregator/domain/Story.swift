//
//  Story.swift
//  NewsAggregator
//
//  Created by Dmitry on 24.03.2022.
//

import Foundation

struct Story: Hashable {
    let title: String
    let subtitle: String
    let imageUrl: String
}
