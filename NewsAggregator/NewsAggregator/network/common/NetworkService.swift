//
//  NetworkService.swift
//  NewsAggregator
//
//  Created by Dmitry on 24.03.2022.
//

import Alamofire
import Combine
import Foundation

enum CustomError: Error {
    case defaultError
}

final class NetworkService<Response> where Response: Decodable {
    
    private let queue = DispatchQueue(label: "custom-queue")
    
    func getRequest(url: String, timeout: TimeInterval) -> AnyPublisher<Response, Error> {
        return AF.request(url, method: .get) { urlRequest in
            urlRequest.timeoutInterval = timeout
        }.validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .publishDecodable(type: Response.self, queue: self.queue)
            .tryMap { response in
                guard let urlResponse = response.response
                        , 200 ..< 300 ~= urlResponse.statusCode
                        , let responseResult = response.value else {
                            throw CustomError.defaultError
                        }
                return responseResult
            }
            .eraseToAnyPublisher()
    }
    
}
