//
//  ImageNetworkService.swift
//  NewsAggregator
//
//  Created by Dmitry on 24.03.2022.
//

import Alamofire
import Combine
import Foundation
import UIKit

enum ImageError: Error {
    case dataIsNil
    case corruptData
}

protocol ImageNetworkService {
    func loadImage(fromUrl url: String) -> AnyPublisher<UIImage, Error>
}

final class DefaultImageNetworkService: ImageNetworkService {
    
    private struct Constants {
        static let timeout = 10.0
    }
    
    func loadImage(fromUrl url: String) -> AnyPublisher<UIImage, Error> {
        AF.request(url, method: .get) { urlRequest in
            urlRequest.timeoutInterval = Constants.timeout
        }.validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .publishData()
            .tryMap { response in
                guard let data = response.data else {
                    throw ImageError.dataIsNil
                }
                guard let image = UIImage(data: data) else {
                    throw ImageError.corruptData
                }
                return image
            }
            .eraseToAnyPublisher()
    }
    
}
