//
//  StoriesList2NetworkService.swift
//  NewsAggregator
//
//  Created by Dmitry on 24.03.2022.
//

import Combine
import Foundation

final class DefaultStoriesList2NetworkService: StoriesListNetworkService {
    
    private static let url = "https://res.cloudinary.com/woodspoonstaging/raw/upload/v1645473019/assignments/mobile_homework_datasource_b_x0farp.json"
    
    private let networkService = NetworkService<StoryResponse>()
    
    func stories() -> AnyPublisher<[Story], Error> {
        networkService.getRequest(url: Self.url, timeout: NetworkRequestConstants.timeout)
            .map { response in
                return response.metadata.innerdata.map({ StoryMapper.model(from: $0) })
            }.eraseToAnyPublisher()
    }
    
}

private struct StoryResponse: Decodable {
    let metadata: StoryMetadata
}

private struct StoryMetadata: Decodable {
    let innerdata: [StoryDto]
}

private struct StoryDto: Decodable {
    let articlewrapper: ArticleWrapper
    let picture: String
}

private struct ArticleWrapper: Decodable {
    let header: String
    let description: String
}

private final class StoryMapper {
    
    static func model(from dto: StoryDto) -> Story {
        return Story(title: dto.articlewrapper.header,
                     subtitle: dto.articlewrapper.description,
                     imageUrl: dto.picture)
    }
    
}
