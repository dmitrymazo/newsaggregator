//
//  StoryNetworkService.swift
//  NewsAggregator
//
//  Created by Dmitry on 24.03.2022.
//

import Combine

protocol StoriesListNetworkService {
    func stories() -> AnyPublisher<[Story], Error>
}

struct NetworkRequestConstants {
    static let timeout = 10.0
}
