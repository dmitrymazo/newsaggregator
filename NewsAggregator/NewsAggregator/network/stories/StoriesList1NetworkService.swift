//
//  StoriesList1NetworkService.swift
//  NewsAggregator
//
//  Created by Dmitry on 24.03.2022.
//

import Combine
import Foundation

final class DefaultStoriesList1NetworkService: StoriesListNetworkService {
    
    private static let url = "https://res.cloudinary.com/woodspoonstaging/raw/upload/v1645473019/assignments/mobile_homework_datasource_a_tguirv.json"
    
    private let networkService = NetworkService<StoryResponse>()
    
    func stories() -> AnyPublisher<[Story], Error> {
        networkService.getRequest(url: Self.url, timeout: NetworkRequestConstants.timeout)
            .map { response in
                return response.stories.map({ StoryMapper.model(from: $0) })
            }.eraseToAnyPublisher()
    }
    
}

private struct StoryResponse: Decodable {
    let stories: [StoryDto]
}

private struct StoryDto: Decodable {
    let title: String
    let subtitle: String
    let imageUrl: String
}

private final class StoryMapper {
    
    static func model(from dto: StoryDto) -> Story {
        return Story(title: dto.title,
                     subtitle: dto.subtitle,
                     imageUrl: dto.imageUrl)
    }
    
}
