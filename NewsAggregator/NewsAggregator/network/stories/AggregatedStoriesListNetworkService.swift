//
//  AggregatedStoriesListNetworkService.swift
//  NewsAggregator
//
//  Created by Dmitry on 24.03.2022.
//

import Combine
import Foundation

final class AggregatedStoriesListNetworkService: StoriesListNetworkService {
    
    private let services: [StoriesListNetworkService]
    
    private let dispatchGroup = DispatchGroup()
    private let queue = DispatchQueue(label: "aggregated-queue")
    private var cancellables = Set<AnyCancellable>()
    private var numberOfErrors = 0
    private var subject = PassthroughSubject<[Story], Error>()
    
    private var array = [Story]()
    
    private func subscribeToServices() {
        reset()
        services.forEach { service in
            subscribe(to: service)
        }
        
        dispatchGroup.notify(queue: queue) {
            let array = self.array
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                guard self.numberOfErrors != self.services.count else {
                    self.subject.send(completion: .failure(CustomError.defaultError))
                    return
                }
                self.subject.send(array)
            }
        }
    }
    
    private func subscribe(to service: StoriesListNetworkService) {
        self.dispatchGroup.enter()
        service.stories().sink { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
                self?.numberOfErrors += 1
                self?.dispatchGroup.leave()
            default:
                break
            }
        } receiveValue: { [weak self] stories in
            self?.queue.sync {
                self?.array += stories
                self?.dispatchGroup.leave()
            }
        }.store(in: &cancellables)
    }
    
    private func reset() {
        cancellables.forEach { $0.cancel() }
        array = []
        numberOfErrors = 0
    }
    
    // MARK: - Internal
    
    func stories() -> AnyPublisher<[Story], Error> {
        subscribeToServices()
        return subject.eraseToAnyPublisher()
    }
    
    // MARK: - Init
    
    init() {
        services = [
            CacheOrNetworkStoryLoader(networkService: DefaultStoriesList1NetworkService(), cachingService: DefaultStoryCacher(serviceName: "service1", staleTimeout: CachingSettings.service1Timeout)),
            CacheOrNetworkStoryLoader(networkService: DefaultStoriesList2NetworkService(), cachingService: DefaultStoryCacher(serviceName: "service2", staleTimeout: CachingSettings.service2Timeout)),
            CacheOrNetworkStoryLoader(networkService: DefaultStoriesList3NetworkService(), cachingService: DefaultStoryCacher(serviceName: "service3", staleTimeout: CachingSettings.service3Timeout)),
        ]
    }
    
}

struct CachingSettings {
    static let service1Timeout: UInt = 15
    static let service2Timeout: UInt = 30
    static let service3Timeout: UInt = 60
}
