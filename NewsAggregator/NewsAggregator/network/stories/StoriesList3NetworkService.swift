//
//  StoriesList3NetworkService.swift
//  NewsAggregator
//
//  Created by Dmitry on 24.03.2022.
//

import Combine
import Foundation

final class DefaultStoriesList3NetworkService: StoriesListNetworkService {
    
    private static let url = "https://res.cloudinary.com/woodspoonstaging/raw/upload/v1645517666/assignments/mobile_homework_datasource_c_fqsu4l.json"
    
    private let networkService = NetworkService<[StoryDto]>()
    
    func stories() -> AnyPublisher<[Story], Error> {
        networkService.getRequest(url: Self.url, timeout: NetworkRequestConstants.timeout)
            .map { response in
                return response.map({ StoryMapper.model(from: $0) })
            }.eraseToAnyPublisher()
    }
    
}

private struct StoryDto: Decodable {
    let topLine: String
    let subLine1: String
    let subline2: String
    let image: String
}

private final class StoryMapper {
    
    static func model(from dto: StoryDto) -> Story {
        return Story(title: dto.topLine,
                     subtitle: dto.subLine1 + dto.subline2,
                     imageUrl: dto.image)
    }
    
}
